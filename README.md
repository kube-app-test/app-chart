# app-chart

Repository dedicated to hold the helm chart of this test app

## Helm package

1. **(OPTIONAL) Create a secret for private container registry**
```sh
kubectl create ns prod
kubectl create secret -n prod docker-registry regcred --docker-server=<your-registry-server> --docker-username=<your-name> --docker-password=<your-pword> --docker-email=<your-email>

example for Docker Hub:
kubectl create secret -n prod docker-registry regcred \
--docker-server=https://index.docker.io/v2/ \
--docker-username=myUser \
--docker-password=myPass \
--docker-email=myEmail@docker.com
```
2. **Install application**
```sh
helm dependency update
tar -xzf charts/common-1.6.1.tgz -C charts/
rm -f charts/common-1.6.1.tgz

helm install -n prod --create-namespace myapp .
```

### If any changes are made
```sh
helm lint .
helm upgrade -n prod -f values.yaml myapp .
```
### To test changes without impact
```sh
helm install --dry-run --debug -n prod -f values.yaml myapp .
```
### To remove
```sh
helm uninstall -n prod myapp
```

##### Best practices
**https://jfrog.com/blog/helm-charts-best-practices/**

##### Traefik ingress controller on-prem
**https://betterprogramming.pub/install-kubernetes-ingress-on-a-raspberry-pi-cluster-e8d5086c5009**

#### Templating paper - MUST WATCH
**https://blog.flant.com/advanced-helm-templating/**